export interface DatosAfectado {
    'daf_id'?: number;
    'caso_id'?: number;
    'iden_id'?: number;
    'gene_id'?: number;
    'dis_id'?: number;
    'prov_id'?: number;
    'can_id'?: number;
    'sec_id'?: number;
    'parr_id'?: number;
    'daf_nombre': string;
    'daf_apellido': string;
    'daf_ci': string;
    'daf_fecha_nacimiento'?: Date;
    'daf_edad': number;
    'daf_direccion': string;
    'daf_telefono': string;
    'daf_email': string;
    'daf_obsevacion': string;
}
