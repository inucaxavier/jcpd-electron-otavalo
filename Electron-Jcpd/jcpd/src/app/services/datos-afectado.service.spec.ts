import { TestBed } from '@angular/core/testing';

import { DatosAfectadoService } from './datos-afectado.service';

describe('DatosAfectadoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DatosAfectadoService = TestBed.get(DatosAfectadoService);
    expect(service).toBeTruthy();
  });
});
