import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DatosAfectado } from '../models/datosAfectado';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DatosAfectadoService {
  // Coneccion al servidor de NODE, DONDE TENEMOS LA BASE DE DATOS
  API_URI = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getDatosAfectados() {
    return this.http.get(`${this.API_URI}/datosafectado`);
  }
  getDatosAfectado(id: string) {
    return this.http.get(`${this.API_URI}/datosafectado/$ {id} `);
  }
  deleteDatosAfectado(id: string) {
    return this.http.delete(`${this.API_URI}/datosafectado/${id}`);
  }

  saveDatosAfectado(datosafectado: DatosAfectado) {
    return this.http.post(`${this.API_URI}/datosafectado`, datosafectado);
  }
  updateDatosAfectado(id: string, updatedDatosAfectado: DatosAfectado) {
    return this.http.put(`${this.API_URI}/datosafectado/${id}`, updatedDatosAfectado);
  }



  // //////-------------------------------------------------///
  // Contribuyentes de otras tablas

  getProco() {
    return this.http.get(`${this.API_URI}/proco`);
  }
  getTipoDenuncia() {
    return this.http.get(`${this.API_URI}/tipoDenuncia`);
  }
  getDerechoAmeViolentado() {
    return this.http.get(`${this.API_URI}/tipoDerechoAmenazas`);
  }
  getTiposDerechosAmenazas() {
    return this.http.get(`${this.API_URI}/amenazadoViolentado`);
  }


}
