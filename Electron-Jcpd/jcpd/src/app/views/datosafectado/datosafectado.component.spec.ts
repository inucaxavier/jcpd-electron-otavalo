import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosafectadoComponent } from './datosafectado.component';

describe('DatosafectadoComponent', () => {
  let component: DatosafectadoComponent;
  let fixture: ComponentFixture<DatosafectadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatosafectadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosafectadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
