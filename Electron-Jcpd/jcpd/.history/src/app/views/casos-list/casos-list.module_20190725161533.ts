import { PageHeaderModule } from '../components/page-header/page-header.module';
import { CasosListComponent } from './casos-list.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CasosListRoutingModule } from './casos-list-routing.module';

@NgModule({
  declarations: [CasosListComponent],
  imports: [
    CommonModule,
    CasosListRoutingModule,
    PageHeaderModule
  ]
})
export class CasosListModule { }
