import { CasosFormComponent } from './casos-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CasosFormRoutingModule } from './casos-form-routing.module';
import { PageHeaderModule } from '../components/page-header/page-header.module';
import { DatosafectadoComponent } from "../datosafectado/datosafectado.component";
@NgModule({
  declarations: [CasosFormComponent],
  imports: [
    CommonModule,
    CasosFormRoutingModule,
    FormsModule,
    CasosFormRoutingModule,
    PageHeaderModule,
    ReactiveFormsModule,
    DatosafectadoComponent
  ]
})
export class CasosFormModule { }
