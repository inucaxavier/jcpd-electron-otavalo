import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Caso } from '../../models/Caso';
import { CasosService } from '../../services/casos.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-casos-form',
  templateUrl: './casos-form.component.html',
  styleUrls: ['./casos-form.component.scss'],
  animations: [routerTransition()]
})
export class CasosFormComponent implements OnInit {


  caso: Caso = {
    caso_id: 0,
    proco_id: 0,
    tda_id: 0,
    dav_id: 0,
    tdenun_id: 0,
    caso_numcaso: '',
    caso_motivo: '',
    caso_observaciones: '',
    caso_fechareg: new Date(),
    caso_fechaingreso: new Date()
  };




  // edit: boolean = false;
  proco: any = [];
  tdenun: any = [];
  tda: any = [];
  ttda: any = [];
  angForm: FormGroup;
  nForm: FormGroup;
  constructor(private casosService: CasosService, private fb: FormBuilder) {
    this.createForm();
  }
  angFor = new FormGroup({
    proco_id: new FormControl('', Validators.required),
    tda_id: new FormControl('', Validators.required),
    dav_id: new FormControl('', Validators.required),
    tdenun_id: new FormControl('', Validators.required),
    caso_numcaso: new FormControl('', Validators.required),
    caso_motivo: new FormControl('', Validators.required),
    caso_observaciones: new FormControl('', Validators.required),
    dacaso_fecharegv_id: new FormControl('', Validators.required),
    caso_fechaingreso: new FormControl('', Validators.required)
  });


  form = new FormGroup({
    first: new FormControl('Nancy', Validators.minLength(2)),
    last: new FormControl('Drew'),
  });

  get first(): any { return this.form.get('first'); }

  onSubmit(): void {
    console.log(this.form.value);  // {first: 'Nancy', last: 'Drew'}
  }

  setValue() { this.form.setValue({ first: 'Carson', last: 'Drew' }); }
}





createForm() {
  this.angForm = this.fb.group({
    proco_id: ['', Validators.required],
    tda_id: ['', Validators.required],
    dav_id: ['', Validators.required],
    tdenun_id: ['', Validators.required],
    caso_numcaso: ['', Validators.required],
    caso_motivo: ['', Validators.required],
    caso_observaciones: ['', Validators.required],
    dacaso_fecharegv_id: ['', Validators.required],
    caso_fechaingreso: ['', Validators.required],

  });
}





ngOnInit() {
  this.nForm = new FormGroup({
    'proco_id': new FormControl(this.caso.proco_id, Validators.required)

  });


  this.casosService.getProco().subscribe(
    res => {
      this.proco = res;
    },
    err => console.error(err)
  );
  this.casosService.getTipoDenuncia().subscribe(
    res => {
      this.tdenun = res;
    },
    err => console.log(err)
  );
  this.casosService.getTiposDerechosAmenazas().subscribe(
    res => {
      this.ttda = res;
    },
    err => console.log(err)
  );
  this.casosService.getDerechoAmeViolentado().subscribe(
    res => {
      this.tda = res;
    },
    err => console.log(err)
  );

}
get proco_id() { return this.nForm.get('proco_id'); }


saveNewCaso() {
  delete this.caso.caso_id;
  this.casosService.saveCaso(this.caso)
    .subscribe(
      res => {
        console.log(res);
      },
      err => console.error(err)
    );
}

}
