import { CasosFormComponent } from './casos-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { CasosFormRoutingModule } from './casos-form-routing.module';
import { PageHeaderModule } from '../components/page-header/page-header.module';

@NgModule({
  declarations: [CasosFormComponent],
  imports: [
    CommonModule,
    CasosFormRoutingModule,
    FormsModule,
    CasosFormRoutingModule,
    PageHeaderModule,
    ReactiveFormsModule,
    FormControl,
    FormGroup,
    FormBuilder
  ]
})
export class CasosFormModule { }
