import { CasosFormComponent } from './casos-form.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CasosFormRoutingModule } from './casos-form-routing.module';
import { PageHeaderModule } from '../../shared';

@NgModule({
  declarations: [CasosFormComponent],
  imports: [
    CommonModule,
    CasosFormRoutingModule,
    FormsModule,
    CasosFormRoutingModule,
    PageHeaderModule,
    ReactiveFormsModule
  ]
})
export class CasosFormModule { }
